package com.sourceit.anroid5;

/**
 * Created by Student on 16.01.2018.
 */


public class Article {

    private String title;
    private String text;
    private String description;


    public Article(String title, String text, String descrition) {
        this.title = title;
        this.text = text;
        this.description = descrition;

    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getDescription() {
        return description;
    }
}


