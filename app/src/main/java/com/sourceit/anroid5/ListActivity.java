package com.sourceit.anroid5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.BindViews;

public class ListActivity extends AppCompatActivity {

    ListView listView;
    BaseArticleAdapter adapter;
    Article[] articles = Generator.generate();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listView = (ListView) findViewById(R.id.list_view);


        adapter = new BaseArticleAdapter(this, Generator.generateList());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListActivity.this, articles[position].getTitle(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
