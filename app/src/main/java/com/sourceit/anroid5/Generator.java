package com.sourceit.anroid5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Student on 16.01.2018.
 */

public final class Generator {

    private Generator() {
    }

    public static Article[] generate() {
        Article[] articles = new Article[10];
        for (int i = 0; i < 10; i++) {
            articles[i] = new Article( "Title " + i,
                    "Long Long Text " + i,
                            "Description "+i
            );
        }
        return articles;
    }

    public static List<Article> generateList() {
        return new ArrayList<>(Arrays.asList(generate()));
    }
}
